'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`<nav>
    <ul class="list">
        <li class="title">
            <a href="index.html" data-type="index-link">radiofrontend documentation</a>
        </li>
        <li class="divider"></li>
        ${ isNormalMode ? `<div id="book-search-input" role="search">
    <input type="text" placeholder="Type to search">
</div>
` : '' }
        <li class="chapter">
            <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
            <ul class="links">
                    <li class="link">
                        <a href="overview.html" data-type="chapter-link">
                            <span class="icon ion-ios-keypad"></span>Overview
                        </a>
                    </li>
                    <li class="link">
                        <a href="index.html" data-type="chapter-link">
                            <span class="icon ion-ios-paper"></span>README
                        </a>
                    </li>
                    <li class="link">
                        <a href="dependencies.html"
                            data-type="chapter-link">
                            <span class="icon ion-ios-list"></span>Dependencies
                        </a>
                    </li>
            </ul>
        </li>
        <li class="chapter modules">
            <a data-type="chapter-link" href="modules.html">
                <div class="menu-toggler linked" data-toggle="collapse"
                    ${ isNormalMode ? 'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                    <span class="icon ion-ios-archive"></span>
                    <span class="link-name">Modules</span>
                    <span class="icon ion-ios-arrow-down"></span>
                </div>
            </a>
            <ul class="links collapse"
            ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                    <li class="link">
                        <a href="modules/AlbumsModule.html" data-type="entity-link">AlbumsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-AlbumsModule-5ea168d4446d9dfb5f4dd4d45b0e913c"' : 'data-target="#xs-components-links-module-AlbumsModule-5ea168d4446d9dfb5f4dd4d45b0e913c"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-AlbumsModule-5ea168d4446d9dfb5f4dd4d45b0e913c"' : 'id="xs-components-links-module-AlbumsModule-5ea168d4446d9dfb5f4dd4d45b0e913c"' }>
                                        <li class="link">
                                            <a href="components/AlbumComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">AlbumComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/AlbumListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">AlbumListComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-AppModule-de77bb16db283c829439f784d1e60002"' : 'data-target="#xs-components-links-module-AppModule-de77bb16db283c829439f784d1e60002"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-AppModule-de77bb16db283c829439f784d1e60002"' : 'id="xs-components-links-module-AppModule-de77bb16db283c829439f784d1e60002"' }>
                                        <li class="link">
                                            <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/IndexPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">IndexPageComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/MenuComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">MenuComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                    </li>
                    <li class="link">
                        <a href="modules/ArtistsModule.html" data-type="entity-link">ArtistsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-ArtistsModule-d39a8965f464522cdd7b70620a9e3014"' : 'data-target="#xs-components-links-module-ArtistsModule-d39a8965f464522cdd7b70620a9e3014"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-ArtistsModule-d39a8965f464522cdd7b70620a9e3014"' : 'id="xs-components-links-module-ArtistsModule-d39a8965f464522cdd7b70620a9e3014"' }>
                                        <li class="link">
                                            <a href="components/ArtistComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">ArtistComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/ArtistListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">ArtistListComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/BandsModule.html" data-type="entity-link">BandsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-BandsModule-d6b971786093c961816709df2314dbf5"' : 'data-target="#xs-components-links-module-BandsModule-d6b971786093c961816709df2314dbf5"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-BandsModule-d6b971786093c961816709df2314dbf5"' : 'id="xs-components-links-module-BandsModule-d6b971786093c961816709df2314dbf5"' }>
                                        <li class="link">
                                            <a href="components/BandComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">BandComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/BandListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">BandListComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/ItemsModule.html" data-type="entity-link">ItemsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' : 'data-target="#xs-components-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' : 'id="xs-components-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' }>
                                        <li class="link">
                                            <a href="components/ItemListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemListComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/ItemSummaryComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemSummaryComponent</a>
                                        </li>
                                </ul>
                            </li>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#injectables-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' : 'data-target="#xs-injectables-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' }>
                                    <span class="icon ion-md-arrow-round-down"></span>
                                    <span>Injectables</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="injectables-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' : 'id="xs-injectables-links-module-ItemsModule-e26b02a5fc57df365e56af3885bc20ec"' }>
                                        <li class="link">
                                            <a href="injectables/ItemsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules"}>ItemsService</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/MenuModule.html" data-type="entity-link">MenuModule</a>
                    </li>
                    <li class="link">
                        <a href="modules/NewsModule.html" data-type="entity-link">NewsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-NewsModule-45ebfc57d28d745183f0bbe8a44065ad"' : 'data-target="#xs-components-links-module-NewsModule-45ebfc57d28d745183f0bbe8a44065ad"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-NewsModule-45ebfc57d28d745183f0bbe8a44065ad"' : 'id="xs-components-links-module-NewsModule-45ebfc57d28d745183f0bbe8a44065ad"' }>
                                        <li class="link">
                                            <a href="components/NewComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/NewListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewListComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/PagesModule.html" data-type="entity-link">PagesModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-PagesModule-117b45aebee06ec5c51036cc6e545ec7"' : 'data-target="#xs-components-links-module-PagesModule-117b45aebee06ec5c51036cc6e545ec7"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-PagesModule-117b45aebee06ec5c51036cc6e545ec7"' : 'id="xs-components-links-module-PagesModule-117b45aebee06ec5c51036cc6e545ec7"' }>
                                        <li class="link">
                                            <a href="components/AlbumPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">AlbumPageComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/ArtistPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">ArtistPageComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/BandPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">BandPageComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/NewPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewPageComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/SongPageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">SongPageComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/PlayersModule.html" data-type="entity-link">PlayersModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-PlayersModule-d558daaca15e91c861711c3419069184"' : 'data-target="#xs-components-links-module-PlayersModule-d558daaca15e91c861711c3419069184"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-PlayersModule-d558daaca15e91c861711c3419069184"' : 'id="xs-components-links-module-PlayersModule-d558daaca15e91c861711c3419069184"' }>
                                        <li class="link">
                                            <a href="components/PlayerBodyComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerBodyComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/PlayerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/PlayerHeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerHeaderComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/PlaylistsModule.html" data-type="entity-link">PlaylistsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-PlaylistsModule-5e5d343673b0c3d28a2d1519fa3ef2d7"' : 'data-target="#xs-components-links-module-PlaylistsModule-5e5d343673b0c3d28a2d1519fa3ef2d7"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-PlaylistsModule-5e5d343673b0c3d28a2d1519fa3ef2d7"' : 'id="xs-components-links-module-PlaylistsModule-5e5d343673b0c3d28a2d1519fa3ef2d7"' }>
                                        <li class="link">
                                            <a href="components/PlaylistComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlaylistComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
                    <li class="link">
                        <a href="modules/TagsModule.html" data-type="entity-link">TagsModule</a>
                            <li class="chapter inner">
                                <div class="simple menu-toggler" data-toggle="collapse"
                                    ${ isNormalMode ? 'data-target="#components-links-module-TagsModule-2dce864d464f86e5fc574c7f84f6b48c"' : 'data-target="#xs-components-links-module-TagsModule-2dce864d464f86e5fc574c7f84f6b48c"' }>
                                    <span class="icon ion-md-cog"></span>
                                    <span>Components</span>
                                    <span class="icon ion-ios-arrow-down"></span>
                                </div>
                                <ul class="links collapse"
                                    ${ isNormalMode ? 'id="components-links-module-TagsModule-2dce864d464f86e5fc574c7f84f6b48c"' : 'id="xs-components-links-module-TagsModule-2dce864d464f86e5fc574c7f84f6b48c"' }>
                                        <li class="link">
                                            <a href="components/TagComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">TagComponent</a>
                                        </li>
                                        <li class="link">
                                            <a href="components/TagListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules">TagListComponent</a>
                                        </li>
                                </ul>
                            </li>
                    </li>
            </ul>
        </li>
        <li class="chapter">
            <div class="simple menu-toggler" data-toggle="collapse"
            ${ isNormalMode ? 'data-target="#classes-links"' : 'data-target="#xs-classes-links"' }>
                <span class="icon ion-ios-paper"></span>
                <span>Classes</span>
                <span class="icon ion-ios-arrow-down"></span>
            </div>
            <ul class="links collapse"
            ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                    <li class="link">
                        <a href="classes/Item.html" data-type="entity-link">Item</a>
                    </li>
            </ul>
        </li>
                <li class="chapter">
                    <div class="simple menu-toggler" data-toggle="collapse"
                        ${ isNormalMode ? 'data-target="#injectables-links"' : 'data-target="#xs-injectables-links"' }>
                        <span class="icon ion-md-arrow-round-down"></span>
                        <span>Injectables</span>
                        <span class="icon ion-ios-arrow-down"></span>
                    </div>
                    <ul class="links collapse"
                    ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                            <li class="link">
                                <a href="injectables/AlbumsService.html" data-type="entity-link">AlbumsService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/ArtistsService.html" data-type="entity-link">ArtistsService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/BandsService.html" data-type="entity-link">BandsService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/BaseService.html" data-type="entity-link">BaseService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/NewsService.html" data-type="entity-link">NewsService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/PlayerService.html" data-type="entity-link">PlayerService</a>
                            </li>
                            <li class="link">
                                <a href="injectables/TagsService.html" data-type="entity-link">TagsService</a>
                            </li>
                    </ul>
                </li>
        <li class="chapter">
            <div class="simple menu-toggler" data-toggle="collapse"
                ${ isNormalMode ? 'data-target="#interfaces-links"' : 'data-target="#xs-interfaces-links"' }>
                <span class="icon ion-md-information-circle-outline"></span>
                <span>Interfaces</span>
                <span class="icon ion-ios-arrow-down"></span>
            </div>
            <ul class="links collapse"
            ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                    <li class="link">
                        <a href="interfaces/Album.html" data-type="entity-link">Album</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/Artist.html" data-type="entity-link">Artist</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/ArtistBand.html" data-type="entity-link">ArtistBand</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/Band.html" data-type="entity-link">Band</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/Configuration.html" data-type="entity-link">Configuration</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/New.html" data-type="entity-link">New</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/ResponseAPI.html" data-type="entity-link">ResponseAPI</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/Tag.html" data-type="entity-link">Tag</a>
                    </li>
                    <li class="link">
                        <a href="interfaces/TagAlbum.html" data-type="entity-link">TagAlbum</a>
                    </li>
            </ul>
        </li>
        <li class="chapter">
            <div class="simple menu-toggler" data-toggle="collapse"
            ${ isNormalMode ? 'data-target="#miscellaneous-links"' : 'data-target="#xs-miscellaneous-links"' }>
                <span class="icon ion-ios-cube"></span>
                <span>Miscellaneous</span>
                <span class="icon ion-ios-arrow-down"></span>
            </div>
            <ul class="links collapse"
            ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                    <li class="link">
                      <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                    </li>
            </ul>
        </li>
            <li class="chapter">
                <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
            </li>
        <li class="chapter">
            <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
        </li>
        <li class="divider"></li>
        <li class="copyright">
                Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.svg" class="img-responsive" data-type="compodoc-logo">
                </a>
        </li>
    </ul>
</nav>`);
        this.innerHTML = tp.strings;
    }
});
