import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Album } from '../shared/album.model';
import { Item } from '../../items/shared/item.model';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {

    @Input() albums: Album[]; //Recibe los albunes de la lista

    @Output() public clickAlbum: EventEmitter<Album> = new EventEmitter<Album>();
    public album: Album;

    constructor() { }

    ngOnInit() {
    }

    onClickAlbum($event: Album) {
        this.album = $event;
        this.clickAlbum.emit($event);
    }

}
