import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumComponent } from './album/album.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AlbumListComponent, AlbumComponent],
  exports: [AlbumListComponent]
})
export class AlbumsModule { }
