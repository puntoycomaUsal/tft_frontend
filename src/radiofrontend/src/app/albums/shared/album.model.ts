import { Item } from '../../items/shared/item.model';

export interface Album {
    id?: number;
    name?: string;
    publication_date?: string;
    web?: string;
    cover?: Item;
    songs?: Item[];
}
