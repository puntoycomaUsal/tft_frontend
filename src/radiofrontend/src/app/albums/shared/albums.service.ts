import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Album } from './album.model';
import { Item } from '../../items/shared/item.model';
import { environment } from '../../../environments/environment';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { BaseService } from '../../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService extends BaseService {

    urlRootApiAlbums = environment.urlRoot+'album';

    constructor(http: HttpClient) {
        super(http,  environment.urlRoot+'album');
    }

    decorateAlbums(albums: Album[]): Observable<Album>[] {
        return albums.map((album: Album) => this.decorateAlbum(album))
    }

    getItemsFromAlbum(albumId: number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiAlbums}/${albumId}/items`);
    }

    decorateAlbum(album: Album): Observable<object> {

        return new Observable((observer) => {
            this.getItemsFromAlbum(album.id)
                .subscribe((response: ResponseAPI) => {
                    album['songs'] = super.filterItems(<Item[]>response.data, 2);
                    album['cover'] = super.filterItems(<Item[]>response.data, 3)[0];
                    observer.next(album)
                    observer.complete()
                })

        })

    }

}
