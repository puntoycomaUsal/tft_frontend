import { MenuComponent } from './menus/menu/menu.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Módulos importados
import { ItemsModule } from './items/items.module';
import { PlayersModule } from './players/players.module';
import { PlaylistsModule } from './playlists/playlists.module';
import { MenuModule } from './menus/menu.module';
import { PagesModule } from './pages/pages.module';
import { TagsModule } from './tags/tags.module';
import { VgCoreModule } from "videogular2/core";
import { VgControlsModule } from "videogular2/controls";
import { VgOverlayPlayModule } from "videogular2/overlay-play";
import { VgBufferingModule } from "videogular2/buffering";

// Componentes importados
import { AppComponent } from './app.component';
import { AppRoutingModule } from './routers/app-routing.module';
import { IndexPageComponent } from './pages/index-page/index-page.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    MenuComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    TagsModule,
    MenuModule,
    ItemsModule,
    PlayersModule,
    PlaylistsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
