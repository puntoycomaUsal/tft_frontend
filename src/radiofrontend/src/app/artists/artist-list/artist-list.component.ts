import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Artist } from '../shared/artist.model';


@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit {

  @Input() artists: Artist[];
  @Output() public clickArtist: EventEmitter<Artist> = new EventEmitter<Artist>();
  public artist: Artist;



  constructor() { }

  ngOnInit() {
  }

  onClickArtist($event: Artist) {
        this.artist = $event;
        this.clickArtist.emit($event);
    }

}
