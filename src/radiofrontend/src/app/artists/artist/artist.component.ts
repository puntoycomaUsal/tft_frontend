import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Artist } from '../shared/artist.model';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  @Input() artist: Artist;
  @Output() public clickArtist: EventEmitter<Artist> = new EventEmitter<Artist>();


  constructor() { }

  ngOnInit() {
  }

  onClickArtist() {
        this.clickArtist.emit(this.artist);
  }

}
