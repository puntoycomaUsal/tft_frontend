import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistListComponent } from './artist-list/artist-list.component';
import { ArtistComponent } from './artist/artist.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ArtistListComponent, ArtistComponent],
  exports: [ArtistListComponent]
})
export class ArtistsModule { }
