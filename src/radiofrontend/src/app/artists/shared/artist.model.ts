import { Item } from '../../items/shared/item.model';

export interface Artist {
    id?: number;
    first_name?: string;
    last_name?: string;
    birth_date?: string;
    birth_place?: string;
    biography?: string;
    web?: string;
    cover?: Item;
}
