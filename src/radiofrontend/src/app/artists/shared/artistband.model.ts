export interface ArtistBand {
    id?: number;
    typeband_id?: number;
    name?: string;
    foundation_date?: string;
    web?: string;
}
