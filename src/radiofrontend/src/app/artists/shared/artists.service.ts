import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Artist } from './artist.model';
import { Item } from '../../items/shared/item.model';
import { environment } from '../../../environments/environment';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { BaseService } from '../../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistsService extends BaseService {

  	urlRootApiArtists     = environment.urlRoot+'artist';

    constructor(http: HttpClient) { 
         super(http,  environment.urlRoot+'artist');
    }

    getArtist(artistId:number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiArtists}/${artistId}`);
    }

    getArtistBand(artistId:number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiArtists}/${artistId}/bands`);
    }

    decorateArtists(artists: Artist[]): Observable<Artist>[] {
        return artists.map((artist: Artist) => this.decorateArtist(artist))
    }

    getItemsFromArtist(artistId: number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiArtists}/${artistId}/items`);
    }

    decorateArtist(artist: Artist): Observable<object> {

        return new Observable((observer) => {
            this.getItemsFromArtist(artist.id)
                .subscribe((response: ResponseAPI) => {
                    artist['songs'] = super.filterItems(<Item[]>response.data, 2);
                    artist['cover'] = super.filterItems(<Item[]>response.data, 3)[0];
                    observer.next(artist)
                    observer.complete()
                })
        })

    }

}