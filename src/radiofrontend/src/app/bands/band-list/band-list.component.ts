import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { Band } from '../shared/band.model';

@Component({
  selector: 'app-band-list',
  templateUrl: './band-list.component.html',
  styleUrls: ['./band-list.component.css']
})
export class BandListComponent implements OnInit {

  @Input() bands: Band[];
  @Output() public clickBand: EventEmitter<Band> = new EventEmitter<Band>();
  public band: Band;

  constructor() { }

  ngOnInit() {
  }

  onClickBand($event: Band) {
        this.band = $event;
        this.clickBand.emit($event);
    }


}
