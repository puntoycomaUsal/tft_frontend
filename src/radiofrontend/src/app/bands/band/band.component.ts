import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Band } from '../shared/band.model';


@Component({
  selector: 'app-band',
  templateUrl: './band.component.html',
  styleUrls: ['./band.component.css']
})
export class BandComponent implements OnInit {

  @Input() band: Band; 
  @Output() public clickBand: EventEmitter<Band> = new EventEmitter<Band>();

  constructor() { }

  ngOnInit() {
  }

  onClickBand() {
        this.clickBand.emit(this.band);
    }
}
