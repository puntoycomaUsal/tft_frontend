import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandListComponent } from './band-list/band-list.component';
import { BandComponent } from './band/band.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BandListComponent,BandComponent],
  exports: [BandListComponent]
})
export class BandsModule { }
