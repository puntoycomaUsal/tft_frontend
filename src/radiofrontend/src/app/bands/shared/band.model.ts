import { Item } from '../../items/shared/item.model';

export interface Band {
    id?: number;
    typeband_id?: number;
    name?: string;
    foundation_date?: string;
    web?: string;
    cover?: Item;
}
