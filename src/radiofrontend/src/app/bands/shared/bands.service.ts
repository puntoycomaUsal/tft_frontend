import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Band } from './band.model';
import { environment } from '../../../environments/environment';
import { ResponseAPI } from '../../shared/responseAPI.model';

@Injectable({
  providedIn: 'root'
})
export class BandsService {

    urlRootApiBands    = environment.urlRoot+'band';

    constructor(private http: HttpClient) { }

    getBands(): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.urlRootApiBands);
    }

    getBandArtist(bandId:number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiBands}/artist/${bandId}`);
    }

}


