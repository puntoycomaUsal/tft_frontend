import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Item } from '../items/shared/item.model';
import { environment } from '../../environments/environment';
import { ResponseAPI } from '../shared/responseAPI.model';
import { Configuration } from '../shared/configuration.model';

/**
 * Servicio base del que pueden extender otros servicios para evitar tener
 * que escribir demasiado código.
 *
 * Se puede consultar un ejemplo de su uso en el servicio de album.
 */

@Injectable()
export class BaseService {

    /**
     * Enpoint configuración
     * @type {string}
     */
    urlRootApiConfiguration = environment.urlRoot+'configuration';
    /**
     * Endpoint principal del servicio que extienda ya sea item, album, etc...
     * @type {string}
     */
    endpoint: string;

    /**
     * Las tres variables declaras a continuación se encargan de hacer accesible los
     * datos de la entidad de configuración en cualquier momento desde cualquier componente
     */

    public configuration: Configuration;
    private configurationSubject: BehaviorSubject<Configuration> = new BehaviorSubject(null);
    readonly configuration$: Observable<Configuration> = this.configurationSubject.asObservable();

    constructor(public http: HttpClient, endpoint: string) {
        this.endpoint = endpoint;
        this.getConfiguration();
    }

    getEntity(id:string): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.endpoint+'id');
    }

    listEntities(): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.endpoint);
    }

    getConfiguration(): void {
        this.http.get<ResponseAPI>(this.urlRootApiConfiguration)
            .subscribe((response: ResponseAPI) => {
                this.configuration = response.data[0];
                this.configurationSubject.next(response.data[0]);
            })
    }

    filterItems(items: Item[], type: number): Item[] {
        return items.filter((item: Item) => {return item.typeitem_id === type})
    }
}
