import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '../shared/item.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  @Input() items: Item[];
  @Input() currentItem: Item;
  @Output() public clickList = new EventEmitter<any>();

  constructor() { }

  public clickOperation(index: number) {

    let evento: any = {};//creo objeto json vacio
    evento.item = this.items[index];
    evento.index = index;
    this.clickList.emit(evento);
  }

  ngOnInit() {
  }

}


