import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '../shared/item.model';
import { ItemsService } from '../shared/items.service';

@Component({
  selector: 'app-item-summary',
  templateUrl: './item-summary.component.html',
  styleUrls: ['./item-summary.component.css']
})
export class ItemSummaryComponent implements OnInit {

  @Input() item: Item;
  @Input() indice: number;
  @Output() public click = new EventEmitter<Item>();

  constructor(private router: Router, private itemsService: ItemsService) { }

  ngOnInit() {
  }

}
