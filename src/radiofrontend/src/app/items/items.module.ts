import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ItemListComponent } from './item-list/item-list.component';
import { ItemSummaryComponent } from './item-summary/item-summary.component';

import { ItemsService } from './shared/items.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [ItemListComponent,ItemSummaryComponent], //Componentes dentro del modulo
  providers: [ItemsService], //Servicios de los que hace uso el modulo
  exports: [ItemListComponent] //Lo que el modulo exporta
})
export class ItemsModule { }
