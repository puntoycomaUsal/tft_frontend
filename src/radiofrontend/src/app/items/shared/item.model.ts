export class Item {
  id: number;
  typeitem_id: number;
  name: string;
  band_id: number;
  url: string;
}
