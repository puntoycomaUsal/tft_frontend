import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Item } from './item.model';
import { environment } from '../../../environments/environment';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { BaseService } from '../../base/base.service';

@Injectable()
export class ItemsService extends BaseService{

    urlRootApi  = environment.urlRoot+'item';
    urlRootApiItemAlbum  = environment.urlRoot+'item/album';

    constructor(http: HttpClient) {
        super(http, environment.urlRoot+'item')
    }

    getitems(): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.urlRootApi);
    }

    getitem(itemId: number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApi}/${itemId}`);
    }

}
