import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { New } from '../shared/new.model';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.css']
})
export class NewListComponent implements OnInit {

  @Input() news: New[];
  @Output() public clickNew: EventEmitter<New> = new EventEmitter<New>();
  public new: New;

  constructor() { }

  ngOnInit() {
  }

  onClickNew($event: New) {
        this.new = $event;
        this.clickNew.emit($event);
    }

}
