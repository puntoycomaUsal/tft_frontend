import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { New } from '../shared/new.model';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  @Input() new: New;
  @Output() public clickNew: EventEmitter<New> = new EventEmitter<New>();


  constructor() { }

  ngOnInit() {
  }

  onClickNew() {
        this.clickNew.emit(this.new);
  }
  
}
