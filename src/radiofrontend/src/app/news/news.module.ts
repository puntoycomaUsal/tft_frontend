import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewListComponent } from './new-list/new-list.component';
import { NewComponent } from './new/new.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NewListComponent, NewComponent],
  exports: [NewListComponent]
})
export class NewsModule { }
