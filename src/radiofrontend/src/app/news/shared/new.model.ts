export interface New {
    id?: number;
    title?: string;
    date?: string;
    description?: string;
}
