import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { New } from './new.model';
import { environment } from '../../../environments/environment';
import { ResponseAPI } from '../../shared/responseAPI.model';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

    urlRootApiNews    = environment.urlRoot+'newcollection';

    constructor(private http: HttpClient) { }

    getNews(): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.urlRootApiNews);
    }

    getNew(newId: number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootApiNews}/${newId}`);
    }

}