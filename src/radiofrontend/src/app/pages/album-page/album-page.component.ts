import { Component, OnInit } from '@angular/core';
import { Album } from '../../albums/shared/album.model';
import { Item } from '../../items/shared/item.model';
import { ItemsService } from '../../items/shared/items.service';
import { AlbumsService } from '../../albums/shared/albums.service';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { Observable } from 'rxjs';
import { PlayerService } from '../../shared/player.service';

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.css']
})
export class AlbumPageComponent implements OnInit {

    public albums: Album[];
    public albumSongs: Item[];
    public currentAlbum: Album;
    public albumConvers: Album[];
    private albumItems: ResponseAPI;

    constructor(private albumsService: AlbumsService,
                private playerService: PlayerService) { }

    ngOnInit() {
        this.albums = [];
        this.getAlbums();
        this.currentAlbum = {};
        this.currentAlbum.cover = undefined;
    }

    getAlbums(): void {
        this.albumsService.listEntities()
            .subscribe((response: ResponseAPI) => {
                this.albumsService.decorateAlbums(response.data).map((obs: Observable<Album>) => this.subscribeAlbum(obs))
            })
    }

    subscribeAlbum(obs: Observable<Album>) {
        obs.subscribe((album: Album) => {
            if (album.cover) {
                album.cover.url = this.albumsService.configuration.urlResources + album.cover.url;
            }
            this.albums.push(album)
        })
    }

    onClickAlbum($event: Album): void {
        this.currentAlbum = $event;
        this.playerService.setPlayList($event.songs);
    }

}
