import { Component, OnInit } from '@angular/core';
import { Artist } from '../../artists/shared/artist.model';
import { ArtistBand } from '../../artists/shared/artistband.model';
import { Item } from '../../items/shared/item.model';
import { ItemsService } from '../../items/shared/items.service';
import { ArtistsService } from '../../artists/shared/artists.service';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { Observable } from 'rxjs';
import { PlayerService } from '../../shared/player.service';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.css']
})
export class ArtistPageComponent implements OnInit {

    public artists: Artist[];
    public artistBand: ArtistBand[];
    public artistSongs: object[];
    public currentArtist: Artist;
    public artistConvers: Artist[];
    private artistItems: ResponseAPI;

    constructor(private artistsService: ArtistsService,
                private playerService: PlayerService) { }

    ngOnInit() {
        this.artists = [];
        this.artistBand = [];
        this.getArtists();
        this.currentArtist = {};
        this.currentArtist.cover = undefined;
    }

    getArtists(): void {
        this.artistsService.listEntities()
            .subscribe((response: ResponseAPI) => {
                this.artistsService.decorateArtists(response.data).map((obs: Observable<Artist>) => this.subscribeArtist(obs))
            })
    }


    getArtistBand(idArtist:number): void {
        this.artistsService.getArtistBand(idArtist)
            .subscribe((response: ResponseAPI) => {
                this.artistBand = response.data;
            })
    }

    subscribeArtist(obs: Observable<Artist>) {
        obs.subscribe((artist: Artist) => {
            if (artist.cover) {
                artist.cover.url = this.artistsService.configuration.urlResources + artist.cover.url;
            }
            this.artists.push(artist)
        })
    }

    onClickArtist($event: Artist): void {
        this.currentArtist = $event;
        this.getArtistBand(this.currentArtist.id);

    }

}
