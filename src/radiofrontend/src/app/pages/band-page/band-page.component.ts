import { Component, OnInit } from '@angular/core';
import { Band } from '../../bands/shared/band.model';
import { Artist } from '../../artists/shared/artist.model';
import { Item } from '../../items/shared/item.model';
import { BandsService } from '../../bands/shared/bands.service';
import { ArtistsService } from '../../artists/shared/artists.service';
import { ResponseAPI } from '../../shared/responseAPI.model';

@Component({
  selector: 'app-band-page',
  templateUrl: './band-page.component.html',
  styleUrls: ['./band-page.component.css']
})
export class BandPageComponent implements OnInit {

  public bands: Band[];
  public currentBand: Band;
  public currentArtist: Artist[];
  public bandArtist: Band[];
  public artistId: number;

  constructor(private bandsService: BandsService,
              private artistService: ArtistsService) { }

  ngOnInit() {
        this.bands = [];
        this.currentArtist = [];
        this.bandArtist = [];
        this.getBands();
    }

    getBands(): void {
        this.bandsService.getBands()
            .subscribe((response: ResponseAPI) => {
                this.bands = response.data;
            })
    }

    getArtistId(): void {
        this.bandsService.getBandArtist(this.currentBand.id)
            .subscribe((response: ResponseAPI) => {
                this.bandArtist = response.data;
            })
    }

    getArtist(id:number): void {
          this.artistService.getArtist(id)
            .subscribe((response: ResponseAPI) => {
                this.currentArtist = response.data;
            })
    }


    onClickBand($event: Band): void {
        this.currentBand = $event;
        this.getArtistId();

        if(this.bandArtist.length !=0){
          this.artistId=this.bandArtist[0]['artist_id'];
          this.getArtist(this.artistId);
        }
    }
}
