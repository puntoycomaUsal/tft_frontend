import { Component, OnInit } from '@angular/core';
import { Tag } from '../../tags/shared/tag.model';
import { TagAlbum } from '../../tags/shared/tagAlbum.model';
import { TagsService } from '../../tags/shared/tags.service';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { Album } from '../../albums/shared/album.model';


@Component({
    selector: 'app-index-page',
    templateUrl: './index-page.component.html',
    styleUrls: ['./index-page.component.css']
})
export class IndexPageComponent implements OnInit {

    public currentTag: Tag;
    public tagAlbum: TagAlbum[];
    public album: Album[];
    public albumId: number;

    constructor(private tagsService: TagsService) { }

    ngOnInit() {

        this.currentTag = {};
        this.tagAlbum = [];
        this.album = [];
    }

    getTagAlbum(idTag:number): void {
        this.tagsService.getTagAlbum(idTag)
            .subscribe((response: ResponseAPI) => {
                this.tagAlbum = response.data;
            })
    }

    getAlbum(idAlbum:number): void {
        this.tagsService.getAlbum(idAlbum)
            .subscribe((response: ResponseAPI) => {
                this.album = response.data;
            })
    }

    onClickTag($event: Tag): void {
        this.currentTag = $event;
        this.getTagAlbum(this.currentTag.id);
        if(this.tagAlbum.length !=0){
          this.albumId=this.tagAlbum[0]['album_id'];
          this.getAlbum(this.albumId);
        }

    }

}