import { Component, OnInit } from '@angular/core';
import { New } from '../../news/shared/new.model';
import { Item } from '../../items/shared/item.model';
import { NewsService } from '../../news/shared/news.service';
import { ResponseAPI } from '../../shared/responseAPI.model';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styleUrls: ['./new-page.component.css']
})
export class NewPageComponent implements OnInit {

    public news: New[];
    public currentNew: New;

    constructor(private newsService: NewsService) { }

    ngOnInit() {
        this.news = [];
        this.getNews();
        this.currentNew = {};
    }

    getNews(): void {
        this.newsService.getNews()
            .subscribe((response: ResponseAPI) => {
                this.news = response.data;
            })
    }

    onClickNew($event: New): void {
        this.currentNew = $event;
    }

}