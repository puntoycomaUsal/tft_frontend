import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongPageComponent } from './song-page/song-page.component';
import { ItemsModule } from '../items/items.module';
import { AlbumsModule } from '../albums/albums.module';
import { AlbumPageComponent } from './album-page/album-page.component';
import { ArtistsModule } from '../artists/artists.module';
import { ArtistPageComponent } from './artist-page/artist-page.component';
import { NewPageComponent } from './new-page/new-page.component';
import { NewsModule } from '../news/news.module';
import { BandsModule } from '../bands/bands.module';
import { BandPageComponent } from './band-page/band-page.component';


@NgModule({
  imports: [
    CommonModule,
    ItemsModule,
    AlbumsModule,
    ArtistsModule,
    BandsModule,
    NewsModule
  ],
  declarations: [SongPageComponent, AlbumPageComponent, ArtistPageComponent, NewPageComponent, BandPageComponent],
  providers: []
})
export class PagesModule { }
