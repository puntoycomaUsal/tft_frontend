import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../items/shared/item.model';

@Component({
    selector: 'app-song-page',
    templateUrl: './song-page.component.html',
    styleUrls: ['./song-page.component.css']
})

export class SongPageComponent implements OnInit {

    @Input() itemsSongs: Item[];
    public currentSong: Item;
    public items: Item[];

    constructor() {
        this.currentSong = new Item();
        this.items = [];
    }

    ngOnInit() {
    }

    onClikSong(event: object):void {
        console.log(event)
    }
}
