import { Component, OnInit, Input,Output, EventEmitter} from '@angular/core';
import { Item } from '../../items/shared/item.model';


@Component({
  selector: 'app-player-body',
  templateUrl: './player-body.component.html',
  styleUrls: ['./player-body.component.css']
})
export class PlayerBodyComponent implements OnInit {

  @Input() showplaylist: boolean;
  @Input() item: Item[];
  @Input() items: Item[];
  @Input() currentItem: Item;
  currentIndex: number;
  @Output() public clickList = new EventEmitter<any>();
  @Output() public closeModal = new EventEmitter<void>();

  constructor() { }

    onClickPlaylistItem(evento: any) {
        this.currentIndex = evento.index;
        this.currentItem = evento.item;
        this.clickList.emit(evento);
    }

    onClickClose(): void {
        this.closeModal.emit();
    }

    ngOnInit() {
    }

}
