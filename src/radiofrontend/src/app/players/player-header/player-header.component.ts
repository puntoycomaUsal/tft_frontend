import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../items/shared/item.model';
import { Album } from '../../albums/shared/album.model';

@Component({
  selector: 'app-player-header',
  templateUrl: './player-header.component.html',
  styleUrls: ['./player-header.component.css']
})
export class PlayerHeaderComponent implements OnInit {

  @Input() item: Item[];
  @Input() currentItem: Item;
  @Input() album: Album;
  @Input() urlImage: string;

  imgPlayerHeader: String;

  constructor() {

  	this.imgPlayerHeader="/assets/img/picture_defecto.png";

  }

  ngOnInit() {
  }

}
