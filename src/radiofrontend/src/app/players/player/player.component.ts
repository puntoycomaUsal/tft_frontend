import { Component, OnInit, Input } from '@angular/core';
import { VgAPI } from 'videogular2/core';
import { Item } from '../../items/shared/item.model';
import { Album } from '../../albums/shared/album.model';
import { ItemsService } from '../../items/shared/items.service';
import { PlayerService } from '../../shared/player.service';
import { ResponseAPI } from '../../shared/responseAPI.model';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  evento: any = {};
  currentIndex = 0; //Para identificar el elemento actual y su posición en la lista de reproducción
  currentItem: Item;
  currentAlbumPhoto: Item;
  api: VgAPI;
  album: Album;
  items: Item[];

  public showPlaylist= false;


    constructor(private itemService:ItemsService,
                private playerService: PlayerService) {
        this.items = [];
    }

    //Cuando el player se inicializa comienza a escuchar los eventos enviados por la etiqueta de video
    onPlayerReady(api: VgAPI) {
        this.api = api;

        //Para reproducir automáticamente los videos, debemos escuchar el evento de metadatos cargados.
        this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(
                                    this.playItem.bind(this));
        //Cuando la etiqueta de video dispara el evento terminado y luego pasar al siguiente video.
        this.api.getDefaultMedia().subscriptions.ended.subscribe(
                                    this.nextItem.bind(this));
    }

    nextItem() {
        this.currentIndex++;
        //Control para hacer un ciclo infinito cuando llegamos al final de la
        //lista de reproducción para poder volver al principio
        if (this.currentIndex === this.items.length) {
            this.currentIndex = 0;
        }
        //Establecemos el elemento actual que actualizará nuestros enlaces en la interfaz de usuario.
        this.currentItem = this.items[this.currentIndex];
    }

    playItem() {
        this.api.play();
    }

    //Configuramos los medios para reproducir en nuestro reproductor Videogular.
    onClickPlaylistItem(evento: any) {
        this.currentIndex = evento.index;
        this.currentItem = evento.item;
    }

    //Metodo para mostrar la play-list
    onClickButtonPlaylist() {
        this.showPlaylist=!this.showPlaylist;
    }

    onClickCloseModal(): void {
      this.showPlaylist = false;
    }


    previousItem() {
        this.currentIndex--;
        //Control para hacer un ciclo infinito cuando llegamos al final de la
        //lista de reproducción para poder volver al principio
        if (this.currentIndex < 0) {
            this.currentIndex = this.items.length-1;
        }
        //Establecemos el elemento actual que actualizará nuestros enlaces en la interfaz de usuario.
        this.currentItem = this.items[this.currentIndex];
    }

    ngOnInit() {
        this.itemService.configuration$
            .subscribe((config: object) => {
                if (config != null) {
                    this.getItems();
                    this.playerService.subscribePlayList()
                        .subscribe((playlist: Item[]) => {
                            this.items = playlist
                            this.items = this.decorateItems(this.items);
                            this.currentIndex = 0;
                            if (this.items.length > 0)  {
                                this.currentItem = this.items[this.currentIndex];
                            }
                        })
                }
            })
        this.currentItem = new Item();
        this.currentItem.name="-";
        this.currentItem.url="/assets/img/picture_defecto.png";
    }


    getItems() {
        this.itemService.getitems()
            .subscribe((items: ResponseAPI) =>
                {
                    this.playerService.setPlayList(<Item[]>items.data)
                })
    }




    decorateItems(items: Item[]): Item[] {

        let urlResources = this.itemService.configuration.urlResources;
        items.forEach((item: Item) => {
            if (!item.url.includes('http')) {
                item.url = urlResources + item.url;
            }
        })

        return items;

    }

}


