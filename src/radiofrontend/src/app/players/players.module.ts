import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { PlayerComponent } from './player/player.component';
import { VgBufferingModule } from 'videogular2/buffering';
import { PlayerHeaderComponent } from './player-header/player-header.component';
import { ItemsModule } from '../items/items.module';
import { PlaylistsModule } from '../playlists/playlists.module';
import { PlayerBodyComponent } from './player-body/player-body.component';


@NgModule({
  imports: [
    ItemsModule,
    PlaylistsModule,
    CommonModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule
  ],
  declarations: [PlayerComponent, PlayerHeaderComponent, PlayerBodyComponent],
  exports: [PlayerComponent]
})
export class PlayersModule { }


