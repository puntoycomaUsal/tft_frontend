import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../items/shared/item.model';


@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  @Input() item: Item[];

  constructor() { }

  ngOnInit() {
  }

}

