import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsModule } from '../items/items.module';
import { PlaylistComponent } from './playlist/playlist.component';


@NgModule({
  imports: [
    CommonModule,
    ItemsModule
  ],
  declarations: [PlaylistComponent],
  exports: [PlaylistComponent]

})
export class PlaylistsModule { }

