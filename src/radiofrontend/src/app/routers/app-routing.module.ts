import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPageComponent } from '../pages/index-page/index-page.component';
import { SongPageComponent } from '../pages/song-page/song-page.component';
import { AlbumPageComponent } from '../pages/album-page/album-page.component';
import { ArtistPageComponent } from '../pages/artist-page/artist-page.component';
import { BandPageComponent } from '../pages/band-page/band-page.component';
import { NewPageComponent } from '../pages/new-page/new-page.component';


const routes: Routes = [
  { path: '', component: IndexPageComponent },
  { path: 'index', component: IndexPageComponent },
  { path: 'songs', component: SongPageComponent },
  { path: 'albums', component: AlbumPageComponent },
  { path: 'artists', component: ArtistPageComponent },
  { path: 'bands', component: BandPageComponent },
  { path: 'news', component: NewPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
