export interface Configuration {
    id?: number;
    pathResources?: string;
    urlResources?: string;
    urlApi?: string;
    endpointsls?: string;
}
