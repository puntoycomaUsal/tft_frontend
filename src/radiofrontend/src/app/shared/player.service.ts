import { Injectable } from '@angular/core';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Item } from '../items/shared/item.model';
/**
 * La función de este servicio es servir de nexo de comunicación entre cualquier componente y el player, por ejemplo
 * para hacer click sobre un album, una canción, un artista... y comenzar con la reprodución de sus canciones.
 */

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

    private playList: BehaviorSubject<Item[]> = new BehaviorSubject([]);
    readonly playList$: Observable<Item[]> = this.playList.asObservable();

    constructor() { }

    /**
     * Emite a todos los suscriptores una nueva lista para mantener sincronizados todos los
     * componentes referentes al player
     * @param {Item[]} items Nueva lista de canciones
     */

    public setPlayList(items: Item[]): void {
        this.playList.next(items);
    }

    public subscribePlayList(): Observable<Item[]> {
        return this.playList$;
    }

}
