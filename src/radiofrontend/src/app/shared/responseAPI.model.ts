export interface ResponseAPI {
    type?: number;
    message?: string;
    data?: object[];
}
