import { Injectable } from '@angular/core';
import { Tag } from './tag.model';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AlbumsService } from '../../albums/shared/albums.service';


@Injectable({
    providedIn: 'root'
})
export class TagsService {

    private urlRootTags: string;

    constructor(private http: HttpClient,private albumsService: AlbumsService) {
        this.urlRootTags = environment.urlRootTags;
    }

    public getTags(): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(this.urlRootTags);
    }

    public getTagAlbum(tagId:number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.urlRootTags}/album/${tagId}`);
    }

    public getAlbum(albumId:number): Observable<ResponseAPI> {
        return this.http.get<ResponseAPI>(`${this.albumsService.urlRootApiAlbums}/${albumId}`);
    }

}
