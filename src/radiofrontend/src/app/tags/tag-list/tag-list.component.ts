import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Tag } from '../shared/tag.model';
import { ResponseAPI } from '../../shared/responseAPI.model';
import { TagsService } from '../shared/tags.service';

@Component({
    selector: 'app-tag-list',
    templateUrl: './tag-list.component.html',
    styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {

    @Output() public clickTag: EventEmitter<Tag> = new EventEmitter<Tag>();

    public tags: Tag[];
    public tag: Tag;


    constructor(private tagsService: TagsService) {
        this.tags = [];
    }

    ngOnInit() {
        this.getTags();
    }

    /**
     * Recoge la respuesta del end-point para obtener los tags, comprueba que la respuesta
     * es correcta (type igual a uno) y carga los datos en la variable tags para que sea cargado
     * en la vista.
     */
    getTags(): void {
        this.tagsService.getTags()
            .subscribe((response: ResponseAPI) => {
                if (response.type === 1) {
                    this.tags = response.data;
                }
            })
    }

    onClickTag($event: Tag) {
        this.tag = $event;
        this.clickTag.emit($event);
    }

}
