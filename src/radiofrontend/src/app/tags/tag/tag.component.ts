import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tag } from '../shared/tag.model';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

    @Input() tag: Tag;
    @Output() public clickTag: EventEmitter<Tag> = new EventEmitter<Tag>();

    constructor() { }

    ngOnInit() {
    }

    onClickTag(): void  {
        this.clickTag.emit(this.tag)
    }
}
