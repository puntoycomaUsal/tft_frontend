import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagComponent } from './tag/tag.component';
import { TagListComponent } from './tag-list/tag-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TagComponent, TagListComponent],
  exports: [TagListComponent]
})
export class TagsModule { }
